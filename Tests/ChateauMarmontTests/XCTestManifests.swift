import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ChateauMarmontTests.allTests),
    ]
}
#endif
