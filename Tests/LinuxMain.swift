import XCTest

import ChateauMarmontTests

var tests = [XCTestCaseEntry]()
tests += ChateauMarmontTests.allTests()
XCTMain(tests)
